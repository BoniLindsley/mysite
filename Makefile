#!/usr/bin/make -f

.PHONY: all build clean open run

all: build

build:
	# Make sure source directory exists.
	mkdir -p public
	# Create files.
	python3 -m src.mysite

clean:
	-$(RM) -dfr public

open:
	xdg-open 'http://localhost:8000/'

run:
	python3 -m http.server -d public
