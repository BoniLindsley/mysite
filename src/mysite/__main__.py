#!/usr/bin/env python3

# Standard libraries.
import argparse
import datetime
import functools
import inspect
import logging
import pathlib
import re
import shutil
import sys
import typing as t

# External libraries.
import jinja2  # Jinja2
import markdown  # Markdown


_logger = logging.getLogger(__name__)


@functools.cache
def get_module_path() -> None | pathlib.Path:
    module_path = inspect.getsourcefile(lambda: None)
    if module_path is None:
        return None
    return pathlib.Path(module_path)


@functools.cache
def get_module_directory() -> pathlib.Path:
    module_path = get_module_path()
    if module_path is None:
        _logger.warning("Unable to determine script directory.")
        return pathlib.Path()
    return module_path.parent


@functools.cache
def get_project_directory() -> pathlib.Path:
    return get_module_directory().parent.parent


def clean_output(*, output_directory: pathlib.Path) -> None:
    try:
        shutil.rmtree(output_directory)
    except FileNotFoundError:
        pass
    except NotADirectoryError as error:
        raise RuntimeError(
            f"Output path exits but it is not a directory: {output_directory}"
        ) from error


def process_as_markdown(
    *,
    input_directory: pathlib.Path,
    output_directory: pathlib.Path,
    relative_input_path: pathlib.Path,
) -> None:
    input_path = input_directory / relative_input_path
    output_path = output_directory / relative_input_path.with_suffix(
        ".html"
    )
    markdown.markdownFromFile(
        extensions=["extra"],
        input=str(input_path),
        output=str(output_path),
    )


class Article(t.TypedDict):
    date: datetime.datetime
    title: str
    url: str


def get_articles(*, input_directory: pathlib.Path) -> list[Article]:
    article_directory = input_directory / "article"
    article_name_regex = re.compile(
        r"(?P<date>\d{4}-\d{2}-\d{2})_(?P<title>.*)\.[^.]*"
    )
    articles: list[Article] = []
    for article_path in reversed(sorted(article_directory.glob("*"))):
        if article_path.is_dir():
            continue
        article_name_match = article_name_regex.match(article_path.name)
        if not article_name_match:
            continue

        date = datetime.datetime.strptime(
            article_name_match["date"], "%Y-%m-%d"
        )
        title = article_name_match["title"]
        url = article_path.relative_to(input_directory).with_suffix(
            ".html"
        )
        articles.append({"date": date, "title": title, "url": str(url)})
    return articles


class Page(t.TypedDict):
    title: str
    url: str


def get_pages(*, input_directory: pathlib.Path) -> list[Page]:
    page_directory = input_directory / "pages"
    page_name_regex = re.compile(r"(?P<title>.*)\.[^.]*")
    pages: list[Page] = []
    for page_path in sorted(page_directory.glob("*")):
        if page_path.is_dir():
            continue
        page_name_match = page_name_regex.match(page_path.name)
        if not page_name_match:
            continue

        title = page_name_match["title"]
        url = page_path.relative_to(input_directory).with_suffix(".html")
        pages.append({"title": title, "url": str(url)})
    return pages


def generate_index(
    *, input_directory: pathlib.Path, output_directory: pathlib.Path
) -> None:
    jinja2_environment = jinja2.Environment(
        loader=jinja2.FileSystemLoader(
            get_module_directory() / "templates"
        )
    )
    jinja2_template = jinja2_environment.get_template("index.html")

    articles = get_articles(input_directory=input_directory)
    pages = get_pages(input_directory=input_directory)
    context = {
        "SITENAME": "My Site",
        "dates": articles,
        "pages": pages,
    }
    jinja2_template.stream(context).dump(
        str(output_directory / "index.html")
    )


def parse_arguments(args: None | list[str] = None) -> argparse.Namespace:
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument(
        "project", default=pathlib.Path(), nargs="?", type=pathlib.Path
    )
    arguments = argument_parser.parse_args(args=args)
    return arguments


def main(args: None | list[str] = None) -> int:
    _logger.setLevel(level=logging.DEBUG)
    arguments = parse_arguments(args=args)

    project_directory = arguments.project
    input_directory = project_directory / "content"
    output_directory = project_directory / "public"
    clean_output(output_directory=output_directory)

    for input_path in input_directory.rglob("*"):
        # Nothing to be processed for directories by themselves.
        if input_path.is_dir():
            continue
        # Ensure parent output directory exists.
        relative_input_path = input_path.relative_to(input_directory)
        output_path = output_directory / relative_input_path
        output_path.parent.mkdir(exist_ok=True, parents=True)
        # Process based on file name extension.
        if relative_input_path.suffix == ".md":
            process_as_markdown(
                input_directory=input_directory,
                output_directory=output_directory,
                relative_input_path=relative_input_path,
            )
        else:
            shutil.copyfile(input_path, output_path)

    generate_index(
        input_directory=input_directory,
        output_directory=output_directory,
    )

    return 0


if __name__ == "__main__":
    sys.exit(main())
